import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Nota } from '../interfaces/Nota';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class NotaService {

  BASE_URL: string = 'http://localhost:3000';

  constructor(private http: HttpClient) { }
 
  getNotas(): Observable<Nota[]>{
    return this.http.get<Nota[]>(`${this.BASE_URL}/api-notas`);
  }

  getNota(id: string): Observable<Nota>{
    return this.http.get<Nota>(`${this.BASE_URL}/api-notas/${id}`);
  }

  crearNota(nota: Nota): Observable<Nota> {
    return this.http.post<Nota>(`${this.BASE_URL}/api-notas/crear`, nota);
  }

  deleteNota(id: string): Observable<Nota> {
    console.log(id);
    return this.http.delete<Nota>(`${this.BASE_URL}/api-notas/delete?notaId=${id}`);
  }

  updateNota(id: string, nota: Nota): Observable<Nota> {
    return this.http.put<Nota>(`${this.BASE_URL}/api-notas/update?notaId=${id}`, nota);
  }

}
