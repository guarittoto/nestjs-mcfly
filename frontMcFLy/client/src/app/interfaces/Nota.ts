export interface Nota {
    _id?: string;
    nombre: string;
    texto: string;
    favorita: boolean;
  
}