import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {NotasListComponent } from './components/nota-list/nota-list.component';
import { NotaFormComponent } from './components/nota-form/nota-form.component';

const routes: Routes = [
  {
    path: '',
    component: NotasListComponent
  },
  {
    path: 'api-notas',
    component: NotasListComponent
  },
  {
    path: 'api-notas/crear',
    component: NotaFormComponent 
  },
  {
    path: 'api-notas/update/:id',
    component: NotaFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
