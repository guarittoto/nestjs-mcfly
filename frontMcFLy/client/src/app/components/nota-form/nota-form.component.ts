import { Component, OnInit } from '@angular/core';
import { Nota  } from 'src/app/interfaces/Nota';
import { NotaService } from 'src/app/services/nota.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nota-form',
  templateUrl: './nota-form.component.html',
  styleUrls: ['./nota-form.component.css']
})
export class NotaFormComponent implements OnInit {

  nota: Nota = {
    nombre: '',
    texto: '', 
    favorita: false, 
  };
  edit: boolean = false;

  constructor(
    private notaService: NotaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.notaService.getNota(params.id)
        .subscribe(
          res => {
            console.log(res);
            this.nota = res;
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  submitNota() {
    console.log(this.nota)
    this.notaService.crearNota(this.nota)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/']);
        },
        err => console.log(err)
      )
  }
  notasFavo(){

    
  }

  updateNota() {
    // delete this.nota._id;
    this.notaService.updateNota(this.nota._id, this.nota)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/api-notas'])
        },
        err => console.log(err)
      )
  }

}
