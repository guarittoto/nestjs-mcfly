import { Component, OnInit } from '@angular/core';
import { NotaService } from 'src/app/services/nota.service';

import { Nota } from '../../interfaces/Nota'
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-nota-list',
  templateUrl: './nota-list.component.html',
  styleUrls: ['./nota-list.component.css']
})
export class NotasListComponent implements OnInit {

  constructor(private notaService: NotaService) { }

  notas: Nota[];
  notaFav: any = []

  ngOnInit() {
    this.getNotas();
  }

  getNotas(): void {
    this.notaService.getNotas()
      .subscribe(
        res => {
          console.log(res);
          // console.log( res.find( nota => nota.favorita = true))

          this.notas = res
        },
        err => console.log(err)
      )
  }


  notasFav(): void {
    this.notaService.getNotas()
    .subscribe(
      res => {
        console.log(res);
        // console.log( res.find( nota => nota.favorita = true))

        this.notaFav = res
        this.notas= this.notaFav.filter(nota => nota.favorita === true  )
        console.log(this.notasFav)
      },
      err => console.log(err)
    )


  }

  deleteNota(id: string): void {
    this.notaService.deleteNota(id)
      .subscribe(
        res => {
          console.log(res);
          this.getNotas();
        },
        err => console.log(err)
      )
  }

}
