import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApiNotasModule } from './api-notas/api-notas.module';
import { MongooseModule } from '@nestjs/mongoose'

const MongoURI = 'mongodb+srv://UserPruebas:123abc456@cluster0-hdpr5.mongodb.net/api-nota?retryWrites=true&w=majority'

@Module({
  imports: [
    ApiNotasModule, 
    MongooseModule.forRoot(  'mongodb://localhost/api-Nota', {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
