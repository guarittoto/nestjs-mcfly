import { Document } from "mongoose";

export interface Nota  extends Document {
    readonly nombre: String;
    readonly texto: String;
    readonly favorita : Boolean;
}