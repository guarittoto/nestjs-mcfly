import { Test, TestingModule } from '@nestjs/testing';
import { ApiNotasService } from './api-notas.service';

describe('ApiNotasService', () => {
  let service: ApiNotasService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiNotasService],
    }).compile();

    service = module.get<ApiNotasService>(ApiNotasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
