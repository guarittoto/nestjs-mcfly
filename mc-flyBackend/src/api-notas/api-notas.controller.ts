import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';
import { CreateNotaDTO, } from './dto/apiNotas.dto';
import { ApiNotasService } from './api-notas.service';
// import { MorganInterceptor } from 'nest-morgan';



@Controller('api-notas')
export class ApiNotasController {

    constructor(private apiNotasService: ApiNotasService) { }

    @Post('/crear')
    async createPost(@Res() res, @Body() createNotaDTO: CreateNotaDTO) {
        // console.log(createNotaDTO);
        const nota = await this.apiNotasService.crearNota(createNotaDTO);
        return res.status(HttpStatus.OK).json({
            messaje: 'La Nota Ha sido Guardada',
            nota

        })
    }

    @Get('/')
    async getNotas(@Res() resp) {
        const notas = await this.apiNotasService.getNotas();
        return resp.status(HttpStatus.OK).json( notas)

    }

    @Get('/:notaId')
    async getNota(@Res() resp, @Param('notaId') notaID) {
        const nota = await this.apiNotasService.getNota(notaID );
        if (!nota) throw new NotFoundException('No existe esa nota');
        return resp.status(HttpStatus.OK).json( nota ) 

    }
    // @UseInterceptors(MorganInterceptor('combined'))
    @Delete('/delete')
    async deleteNota(@Res() resp,@Query('notaId') notaID ){
        const delnota = await this.apiNotasService.deleteNota(notaID );
        if (!delnota) throw new NotFoundException('No existe esa nota');
        return resp.status(HttpStatus.OK).json({
            messaje: 'Nota Eliminada',
            delnota

        }) 

    }

    @Put('/update')
    async updateNota(@Res() resp, @Body() createNotaDTO: CreateNotaDTO, @Query('notaId') notaId ){
        const updatenota = await this.apiNotasService.updateNota( notaId, createNotaDTO);
        if (!updatenota) throw new NotFoundException('No existe esa nota');
        return resp.status(HttpStatus.OK).json({
            messaje: 'Nota Actualizada',
            updatenota

        }) 

    }


}
