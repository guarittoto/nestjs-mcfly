import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Nota } from './interfaces/apiNota.interface';
import { CreateNotaDTO } from './dto/apiNotas.dto';
import { promises } from 'dns';


@Injectable()
export class ApiNotasService {
    constructor(@InjectModel('Nota') readonly notaModel: Model<Nota>) { }

    async getNotas(): Promise<Nota[]> {
        const notas = await this.notaModel.find()
        return notas;
    }
    async getNota(notaId: string): Promise<Nota> {
        const nota = await this.notaModel.findById(notaId);
        return nota
    }
    async crearNota(crearNotaDTO: CreateNotaDTO): Promise<Nota> {
        const newNota = new this.notaModel(crearNotaDTO);
        return newNota.save();
    }
    async deleteNota(notaId: string): Promise<Nota> {
        const deletenota = await this.notaModel.findByIdAndDelete(notaId);
        return deletenota;
    }

    async updateNota(notaId: string, crearNotaDTO: CreateNotaDTO): Promise<Nota> {
        const updatenota = await this.notaModel.findByIdAndUpdate(notaId, crearNotaDTO, { new: true});
        return updatenota;

}

}
