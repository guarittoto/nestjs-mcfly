import { Module } from '@nestjs/common';
import { ApiNotasController } from './api-notas.controller';
import { ApiNotasService } from './api-notas.service';
import { MongooseModule } from '@nestjs/mongoose';
import { NotaSchema } from './schemas/apiNotas.schema';
// import { MorganModule } from 'nest-morgan';

@Module({
  imports: [
    // MorganModule.forRoot(),
    MongooseModule.forFeature([{ name: 'Nota', schema: NotaSchema }])
  ],
  controllers: [ApiNotasController],
  providers: [ApiNotasService],

})
export class ApiNotasModule { }
