import { Test, TestingModule } from '@nestjs/testing';
import { ApiNotasController } from './api-notas.controller';

describe('ApiNotas Controller', () => {
  let controller: ApiNotasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiNotasController],
    }).compile();

    controller = module.get<ApiNotasController>(ApiNotasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
